export const checkForm = (formRef, successMsg = null) => {
  return new Promise((resolve) =>
    formRef.value?.validate((errors) => {
      if (!errors) {
        resolve([successMsg || true, null])
      } else {
        resolve([null, errors[0][0].message])
      }
    }),
  )
}
