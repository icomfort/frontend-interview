import axios from "axios";
import qs from "qs";

const clientId = "1657362868";
const redirectUri = "https://68dd-124-6-9-19.ngrok.io/login";
const clientSecret = "ce4456920db45116c16ec09715f1ff70";

export const GetLineAuthLink = async () => {
  // code here
  const link = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${clientId}&redirect_uri=${redirectUri}&state=12345abcde&scope=profile%20openid&nonce=09876xyz`;
  return link;
};

export const GetLineAccessToken = async (code) => {
  // code here
  const url = "https://api.line.me/oauth2/v2.1/token";
  const grantType = "authorization_code";
  const data = qs.stringify({
    grant_type: grantType,
    code,
    redirect_uri: redirectUri,
    client_id: clientId,
    client_secret: clientSecret,
  });

  const config = {
    headers: {
      "Content-type": "application/x-www-form-urlencoded;charset=utf-8",
    },
  };

  const res = await axios.post(url, data, config);
  const { access_token: accessToken } = res.data;
  return accessToken;
};

export const GetLineMeProfile = async (accessToken) => {
  // code here
  const url = "https://api.line.me/v2/profile";
  const config = {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  const res = await axios.get(url, config);
  return res.data;
};
